from incolume.py.xpto.inanis import div, ourdiv
import pytest


@pytest.mark.parametrize(
    'entrance expected'.split(),
    (
        ((1, 2), .5),
        ((8, 2), 4),
        ((8, 8), 1),
        ((8, 0), None),
    )
)
def test_div(entrance, expected):
    try:
        assert div(*entrance) == expected
    except ZeroDivisionError:
        with pytest.raises(ZeroDivisionError, match='division by zero'):
            assert div(*entrance)


@pytest.mark.parametrize(
    'entrance expected'.split(),
    (
        ((1, 2), .5),
        ((8, 2, 2), 2),
        ((8, 2, 2, 2), 1),
        ((4, 2, 2, 2), .5),
        ((0, 2, 2, 2), 0),
        ((4, 2, 2, 2, 0), None),
    )
)
def test_ourdiv(entrance, expected):
    assert ourdiv(*entrance) == expected


