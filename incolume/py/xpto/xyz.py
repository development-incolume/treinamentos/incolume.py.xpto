from collections import namedtuple

pessoa = namedtuple('Pessoa', ['nome_completo', 'data_de_nascimento', 'cpf'])


object0 = pessoa('alfredo gustavo alves','06/02/2003', '071.997.373-42')
object1 = pessoa('gustalf perreira melo', '01/02/2003', '071.997.373-45')
object3 = pessoa('henrique alves gomes', '01/02/2023', '071.997.373-48')

pessoas = [object0, object1, object3]

print(object1)
print(object0)

for p in pessoas:
    print(p.data_de_nascimento)
