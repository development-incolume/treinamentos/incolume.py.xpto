from toml import load
from pathlib import Path

proj = Path(__file__).resolve().parents[3]/'pyproject.toml'
versionfile = Path(__file__).absolute().parent / 'version.txt'
try:
    with proj.open() as file:
        versionfile.write_text(f"{load(file)['tool']['poetry']['version']}\n")
except FileNotFoundError:
    pass

__version__ = versionfile.read_text().strip()
