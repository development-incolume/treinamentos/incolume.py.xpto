# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = "@britodfbr"  # pragma: no cover

from collections import namedtuple

pessoa = namedtuple('Pessoa', ['nome_completo', 'data_de_nascimento', 'cpf'])
