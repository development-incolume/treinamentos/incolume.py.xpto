import logging
from pathlib import Path
from typing import Generator
from sqlite3 import connect

import pandas as pd

path = Path(__file__).parents[3]
files = path.joinpath('data').glob('resultado[0-9]*.json')

con = connect(path.joinpath('data', 'database.sqlite'))

# print(files)
df = pd.read_json(next(files))


def concat_df(df: pd.DataFrame, infiles: Generator):
    for file in infiles:
        df = pd.concat((df, pd.read_json(file)), ignore_index=True)
    return df


def json2df(f_json: (Path | str)):
    f_json = f_json or path.joinpath('data', 'resultado.json')
    return pd.read_json(f_json)


def df2sql(df: pd.DataFrame, table: str = None) -> bool:
    table = table or 'leis_senado'
    df.to_sql(table, con=con, index=False, if_exists='replace')
    return True


if __name__ == '__main__':
    ds = concat_df(df, files)
    logging.debug(ds.info())
    json_file = path.joinpath('data', 'resultado.json')
    ds.to_json(json_file, indent=4, orient='records')
    df2sql(ds)
