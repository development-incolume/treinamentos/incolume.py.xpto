from functools import reduce


def div(a, b) -> float:
    return a / b


def ourdiv(*args):
    try:
        return reduce(div, args)
    except ZeroDivisionError:
        return None

