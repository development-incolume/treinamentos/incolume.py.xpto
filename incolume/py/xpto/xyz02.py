import pathlib
from incolume.py.xpto.models.pessoa import pessoa

# import .settings

object0 = pessoa('alfredo gustavo alves', '06/02/2003', '071.997.373-42')
object1 = pessoa('gustalf perreira melo', '01/02/2003', '071.997.373-45')
object3 = pessoa('henrique alves gomes', '01/02/2023', '071.997.373-48')

pessoas = [object0, object1, object3]


def persistir_pessoa0():
    path = pathlib.Path(__file__).absolute().parents[3].joinpath('data')

    arquivo_de_saida = path / 'pessoas.txt'

    # print(path.parent.parent.parent.parent.joinpath('data').exists())

    # for i in path.parents:
    #    print(i)

    # print(path.parents[3].joinpath('data'))

    # print(path.joinpath('..\..\..\..\data').resolve())

    # print(path.joinpath('..', '..', '..', '..', 'data').resolve())
    # print(arquivo_de_saida)

    # arquivo_de_saida.write_text('teste')

    string = ''

    for person in pessoas:
        # print(f'NOME: {person.nome_completo}')
        # print(f'DATA: {person.data_de_nascimento}')
        # print(f'CPF: {person.cpf}')

        string = string + (f'NOME: {person.nome_completo} \n'
                           f'DATA: {person.data_de_nascimento} \n'
                           f'CPF: {person.cpf} \n'
                           '------------------------------\n')

    print(string)

    arquivo_de_saida.write_text(string)


def persistir_pessoa(
    lista_pessoas: list = None, arquivo_de_saida: pathlib.Path = None
):
    lista_pessoas = lista_pessoas or pessoas
    arquivo_de_saida = arquivo_de_saida or \
                       pathlib.Path(__file__).absolute() \
                           .parents[3].joinpath('data') / 'pessoas.txt'
    string = ''
    for person in lista_pessoas:
        string = string + (f'NOME: {person.nome_completo} \n'
                           f'DATA: {person.data_de_nascimento} \n'
                           f'CPF: {person.cpf} \n'
                           '------------------------------\n')
    print(string)
    arquivo_de_saida.write_text(string)


if __name__ == '__main__':    # pragma: no cover
    persistir_pessoa()
