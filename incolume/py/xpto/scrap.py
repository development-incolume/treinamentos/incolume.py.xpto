from collections import namedtuple
from pprint import pprint

from bs4 import BeautifulSoup
import requests
import httpx
import logging

from pathlib import Path
from dataclasses import dataclass
from math import ceil
import json
import time
import random


logFormat = '%(asctime)s; %(levelname)-8s; %(name)s; %(module)s;' \
            ' %(funcName)s; %(threadName)s; %(thread)d; %(message)s'
logging.basicConfig(level=logging.DEBUG, format=logFormat)

URL = f'https://www.camara.leg.br/legislacao/busca?' \
      '&ordenacao=relevancia:DESC' \
      '&abrangencia=Legisla%C3%A7%C3%A3o%20federal' \
      '&tipo=Lei%20Complementar,' \
      'Lei%20Ordin%C3%A1ria,Lei,Lei%20Constitucional,' \
      'Lei%20Delegada,Lei%20Sem%20N%C3%BAmero' \
      '&pagina={}'


def get_page(page: int, url: str = None) -> list:
    url = url or URL

    # logging.info(url)
    logging.debug(url.format(page))

    API_WEB = httpx

    # req = requests.get(url.format(page))
    req = API_WEB.get(url.format(page))
    content = req.content
    # logging.debug(content)

    soup = BeautifulSoup(content, 'html5lib')

    logging.debug(soup.head.meta)
    logging.debug(soup.title.text)

    Path(__file__).parent.joinpath('title.txt')\
        .write_text(soup.title.text, encoding='utf-8')

    result = soup.select('h3.busca-resultados__cabecalho>a')
    logging.debug(f'{type(result)}')
    logging.debug(f'{len(result)}')
    logging.debug(f'{result}')

    result = [(e.text.strip(), e.get('href', '-')) for e in result]
    logging.debug(f'{result}')

    ementa = soup.select('p.busca-resultados__descricao')
    logging.debug(f'{ementa}')

    ementa = [e.get_text().strip().replace('Ementa: ', '') for e in ementa]
    logging.debug(f'{ementa}')

    atos = []

    for (a, b), c in zip(result, ementa):
        ato = {
            'epigrafe': a,
            'ementa': c,
            'url': b
        }
        atos.append(ato)

    print('>>>')
    pprint(atos)
    fout = Path('resultado.json')

    with fout.with_stem(f'{fout.stem}{page:05}').open('w') as fo:
        json.dump(atos, fo, indent=4)


def download_page(initialpage: int = 0):
    r = httpx.get(URL.format(1))
    soup = BeautifulSoup(r.content, 'html5lib')
    total_consulta = int(soup.find('i').text.split()[-1])
    logging.info(total_consulta)
    
    for page in range(initialpage, int(total_consulta / 20 + 2)):
        pausa = random.randint(5, 30)
        logging.info(f'{pausa=}')
        time.sleep(pausa)
        get_page(page)

if __name__ == '__main__':
    
    download_page(501)
    pass





















