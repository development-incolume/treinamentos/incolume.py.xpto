from os import sep

var1 = '123'
var2 = "palavra"

print(
    var1.isdigit(),  # verifica se a varialvel é um digito
    var2.isdigit(),  # verifica se a varialvel é um digito
    var1.isalpha(),  # verifica se a variavel é alfanumerica
    var2.isalpha(),  # verifica se a variavel é alfanumerica
    var1 + var2,  # soma de duas variaveis (concatenação)
    var1 + ' ' + var2,  # contatenação de duas variaveis com um espaço em
    # branco entre elas
    var2.upper(),  # altera a variavel; deixa em maiusculo
    var2.lower(),  # altera a variavel; deixa em minusculo *não funciona para
                   # todas as linguas only ascii
    var2.casefold(),  # multilingual - funciona unicode
    'numero: ' + var1,
    'text: ' + var2,
    'numero: %s; text; %s' % (var1, var2),
    'numero: {}; text; {}'.format(var1, var2),
    f'numero: {var1}; text; {var2}',

    sep='\n'
)
