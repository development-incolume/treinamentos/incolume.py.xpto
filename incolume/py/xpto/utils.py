from pathlib import Path


def rename_files():
    files = Path().glob('resultado[0-9]+.json')
    for file in files:
        n = f"{int(file.stem.split('o')[-1]):05}"
        file.rename(f'resultado{n}.json')
