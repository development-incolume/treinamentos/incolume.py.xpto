import sys
import pathlib

path = pathlib.Path(__file__).parent # pegando o path da raiz e armazenando na variavel
sys.path.append(str(path.as_posix())) # adicionando o path da raiz ao path do sistema
